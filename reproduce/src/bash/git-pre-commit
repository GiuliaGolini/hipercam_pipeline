#!@BINDIR@/bash
#
# The example hook script to store the metadata information of version
# controlled files (with each commit) using the `metastore' program.
#
# Copyright (C) 2018-2019 Mohammad Akhlaghi.
#
# This script is taken from the `examples/hooks/pre-commit' file of the
# `metastore' package (installed within the pipeline, with an MIT license
# for copyright). We have just changed the name of the `MSFILE' and also
# set special characters for the installation location of meta-store so our
# own installation is found by Git.
#
# WARNING:
#
# If the commit is aborted (e.g. by not entering any synopsis),
# then updated metastore file (.metadata by default) is not reverted,
# so its new version remains in the index.
# To undo any changes in metastore file written since HEAD commit,
# you may want to reset and checkout HEAD version of the file:
#
#     git reset HEAD -- .metadata
#     git checkout HEAD -- .metadata

# File containig the metadata and metastore executable.
MSFILE=".file-metadata"
MSBIN=@BINDIR@/metastore

# If metastore is not installed, then ignore this script (exit with a
# status of 0).
if [ ! -f $MSBIN ]; then exit 0; fi

# Function to help in reporting a crash.
exit_on_fail() {
	"$@"
	if [ $? -ne 0 ]; then
		echo "Failed to execute: $@" >&2
		exit 1
	fi
}

# Run metastore.
exit_on_fail \
	$MSBIN -O @USER@ -G @GROUP@ -s -f "$MSFILE"

# If it's first metastore commit, store again to include $MSFILE in $MSFILE.
if ! git-ls-tree --name-only HEAD 2>/dev/null | grep -Fqx "$MSFILE"; then
	exit_on_fail \
	        $MSBIN -O @USER@ -G @GROUP@ -s -f "$MSFILE"
fi

# Check if the metadata file exists.
if [ ! -e "$MSFILE" ]; then
	echo "\"$MSFILE\" missing" >&2
	exit 1
fi

# Add the metadata file to the Git repository.
exit_on_fail \
	git-add "$MSFILE"

# Return with a success code (0).
exit 0
