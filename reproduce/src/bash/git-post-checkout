#!@BINDIR@/bash
#
# The example hook script to store the metadata information of version
# controlled files (with each commit) using the `metastore' program.
#
# Copyright (C) 2018-2019 Mohammad Akhlaghi.
#
# This script is taken from the `examples/hooks/pre-commit' file of the
# `metastore' package (installed within the pipeline, with an MIT license
# for copyright). We have just changed the name of the `MSFILE' and also
# set special characters for the installation location of meta-store so our
# own installation is found by Git.

# File containig the metadata and metastore executable.
MSFILE=".file-metadata"
MSBIN=@BINDIR@/metastore

# If metastore is not installed, then ignore this script (exit with a
# status of 0).
if [ ! -f $MSBIN ]; then exit 0; fi

# Delete all temporary files
find @TOP_PROJECT_DIR@/ -name "*~" -type f -delete

# Function to help in reporting a crash.
exit_on_fail() {
	"$@"
	if [ $? -ne 0 ]; then
		echo "Failed to execute: $@" >&2
		exit 1
	fi
}

# Check if the metadata file exists.
if [ ! -e "$MSFILE" ]; then
	echo "\"$MSFILE\" missing" >&2
	exit 1
fi

# Run metastore.
exit_on_fail \
       $MSBIN -a -m -e -E -q -O @USER@ -G @GROUP@ -f "$MSFILE"

# Return with a success code (0).
exit 0
