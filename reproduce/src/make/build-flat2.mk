# Build the second version of the flat
#
# This Makefile is written to build the second flat for the raw images taken from HiPERCAM instrument.
# The main goal is to have a template to make the second version of flat in each of the four window in every CCD cameras of HiPERCAM.
# This makefile firstly creates a cube image containing the second flat.
# This flat image is created by combining with a mean the masked and normalized images in each CCD's window.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Define the directory where the buid images are going to be
bf2-outdir = $(BDIR)/flat2





# Define the directory where the images from HiPERCAM corrected by bias are taken
bf2-indir = $(BDIR)/corrected-bias





# Define a temporary directory where putting the masked images
#bf2-masked-tmpdir = $(BDIR)/bf2-masked
# Define a temporary directory where putting the masked images normalized
#bf2-masknorm-tmpdir = $(BDIR)/bf2-masknorm



# Define that all the images are taken from the shell in the directory indir, all the files ending with .fits
bf2-inputs = $(shell cd $(bf2-indir) && ls *.fits)





# Now define a sub-target. To create the final text file we have to use the masked images.
# This works as a loop: foreach raw-image in the
# set of bf2-inputs writes in the outdirectory the name of the masked image substituting  
# the current name of the raw data to the extension _masked.fits.
masked-images = $(foreach bf2-input,$(bf2-inputs),$(bf2-outdir)/$(subst .fits,_masked.fits,$(bf2-input)))
# print the names of the masked images
# $(info $(masked-images))




# Masking the images
# ----------------
#
# To create masked images we need the bias corrected images and the masks.
# Create a output directory
$(bf2-outdir): ; mkdir $@
$(masked-images):$(bf2-outdir)/%_masked.fits: $(bf2-indir)/%.fits $(BDIR)/mask/%_det.fits | $(bf2-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# masked data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the masked image
	# Masking each image by using astarithmetic.
	# Inject the masked data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 20);                                                      \
	do                                                                          \
	tmpim=$(subst _masked.fits,_masked-ext$$n.fits,$@)                          \
	&& astarithmetic $^ --hdu=$$n --hdu=$$n  1 eq nan where --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@                                     \
	&& rm $$tmpim                                                               \
	;done






# This is as a loop: foreach bf2-input image in the set of bf2-inputs writes in the outdirectory the name 
# of the normalized image substituting to the current
# name of the masked data the extension _masknorm.fits.
masknorm-images = $(foreach bf2-input,$(bf2-inputs),$(bf2-outdir)/$(subst .fits,_masknorm.fits,$(bf2-input)))





# Normalize the masked images
# ----------------
#
# We use normalized images as variables to create the flat.
# To create the normalized images we need the masked ones taken from outdirectory.

$(masknorm-images): $(bf2-outdir)/%_masknorm.fits: $(bf2-outdir)/%_masked.fits
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# normalized data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the image normalized
	# vnorm = normalization value obtained with the median
	# Normalize each image by dividing itself by vnorm
	# Inject the normalized data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 20);                                          \
	do                                                              \
	tmpim=$(subst _masknorm.fits,_masknorm-ext$$n.fits,$@)          \
	&& vnorm=$$(aststatistics $< --hdu=$$n --median)                \
	&& astarithmetic $< --hdu=$$n $$vnorm / --output=$$tmpim        \
	&& astfits $$tmpim --copy=1 --output=$@                         \
	&& rm $$tmpim                                                   \
	; done






# Build the second version of the flat
# ----------------
#
# To create the 2flats we use the masked normalized images

$(bf2-outdir)/flats2.fits: $(masknorm-images)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# flat data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the flat image
	# Building the flat by using astarithmetic.
	# Inject the flat data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 20);                                                     \
	do                                                                         \
	tmpim=flat2-$$n.fits                                                       \
	&& astarithmetic $^ -g$$n $(words $^) 3 0.2 sigclip-mean --output=$$tmpim                \
	&& astfits $$tmpim --copy=1 --output=$@                                    \
	&& rm $$tmpim                                                              \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/build-flat2.tex: $(bf2-outdir)/flats2.fits
	touch $@
