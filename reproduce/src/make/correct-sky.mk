# Sky correction
#
# This Makefile is written to subctract the sky from the gain corrected images taken from HiPERCAM instrument.
# The main goal is to have a template to subctract the sky in each CCD cameras of HiPERCAM.
# To do that, a loop for reading all extensions is used.
# This routine is used because the sky area covered is very small and assuming there is no gradient for the sky value.
# Then the makefile create  cube images containing the final images.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Define the directory where the buid images are going to be
cs-outdir = $(BDIR)/corrected-sky





# Define the directory where the corrected for bias images from HiPERCAM are taken
cs-indir = $(BDIR)/corrected-gain






# Define that all the cs-inputs  are taken from the shell in the directory indir, all the files ending with _cgain.fits
cs-inputs = $(shell cd $(cs-indir) && ls *_cgain.fits)





# Now defining a sub-target. To create the final text file we have to use the masked images.
# This works as a loop: foreach cs-input in the set of cs-inputs writes in the outdirectory the name of the mask image substituting the current
# name of the raw data with the extension _det.fits.
cs-mask-images = $(foreach cs-input,$(cs-inputs),$(cs-outdir)/$(subst _cgain.fits,_det.fits,$(cs-input)))




# Build the mask of the CCD cameras images.
# ----------------
#
# We use mask-images as variables.
# To create the mask images we need the corrected for gain ones.
$(cs-outdir): ; mkdir $@
$(cs-mask-images): $(cs-outdir)/%_det.fits: $(cs-indir)/%_cgain.fits |$(cs-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# mask data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the mask image
	# Create the mask of each image by using astnoisecheisel.
	# Note that the mask is the second output of astnoisecheisel and it is the only important image needed.
	# Inject the mask data (tmpim, hdu=2) into the target
	# Remove the temporal image
	# So now add hdu to the cube frame and at the same time read each hdu in the prerequisite image.
	for n in $$(seq 1 5);                                  \
	do                                                     \
	tmpim=$(subst _det.fits,_det-ext$$n.fits,$@)           \
	&& astnoisechisel $< --hdu=$$n --output=$$tmpim        \
	&& astfits $$tmpim --copy=2 --output=$@                \
	&& rm $$tmpim                                          \
	;done




# Define that all the gain corrected-images are taken from the shell in the directory indir, all the files ending with cgain.fits
# cs-inputs = $(shell cd $(cs-indir) && ls *_cgain.fits)





# Now define a sub-target. To create the final text file we have to use the masked images.
# This works as a loop: foreach cs-input in the
# set of cs-inputs writes in the outdirectory the name of the masked image substituting  
# the current name of the data to the extension _masked.fits.
cs-masked-images = $(foreach cs-input,$(cs-inputs),$(cs-outdir)/$(subst _cgain.fits,_masked.fits,$(cs-input)))




# Mask the images
# ----------------
#
# To create masked images we need the corrected images and the masks.
$(cs-masked-images):$(cs-outdir)/%_masked.fits: $(cs-indir)/%_cgain.fits $(cs-outdir)/%_det.fits
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# masked data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the masked image
	# Masking each image by using astarithmetic.
	# Inject the masked data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 5);                                                       \
	do                                                                          \
	tmpim=$(subst _masked.fits,_masked-ext$$n.fits,$@)                          \
	&& astarithmetic $^ --hdu=$$n --hdu=$$n  1 eq nan where --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@                                     \
	&& rm $$tmpim                                                               \
	;done





# This is as a loop: foreach cs-input image in the set of cs-inputs writes in the outdirectory the name 
# of the normalized image substituting to the current
# name of the masked data the extension _masknorm.fits.
cs-masknorm-images = $(foreach cs-input,$(cs-inputs),$(cs-outdir)/$(subst _cgain.fits,_masknorm.fits,$(cs-input)))




# Normalize the masked images
# ----------------
#
#We use normalized images as variables to create the sky.
#To create the normalized images we need the masked ones.
$(cs-masknorm-images): $(cs-outdir)/%_masknorm.fits: $(cs-outdir)/%_masked.fits
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# normalized data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the image normalized
	# vnorm = normalization value obtained with the median
	# Normalize each image by dividing itself by vnorm
	# Inject the normalized data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 5);                                       \
	do                                                          \
	tmpim=$(subst _masknorm.fits,_masknorm-ext$$n.fits,$@)      \
	&& vnorm=$$(aststatistics $< --hdu=$$n --median)            \
	&& astarithmetic $< --hdu=$$n $$vnorm / --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@                     \
	&& rm $$tmpim                                               \
	; done





# This works as a loop: foreach cs-input image in the set of cs-inputs writes in the outdirectory the name 
# of the sky image substituting to the current
# name of the  data the extension _csky.fits
csky-images = $(foreach cs-input,$(cs-inputs),$(cs-outdir)/$(subst _cgain.fits,_csky.fits,$(cs-input)))




# Correct the image for the sky value
#--------------
#
#We use csky-images as variables.
#To create the corrected sky images we need the gain corrected ones and the masked ones.
$(csky-images): $(cs-outdir)/%_csky.fits: $(cs-indir)/%_cgain.fits $(cs-outdir)/%_masked.fits
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# sky data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the sky image 
	# vsky = median value of the pixels of the masked image.
	# Correct the image by subtracting the median value to each image
	# Inject the sky data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 5);                                       \
	do                                                          \
	tmpim=$(subst _csky.fits,_csky-ext$$n.fits,$@)              \
	&& vsky=$$(aststatistics $(word 2,$^) --hdu=$$n --median)   \
	&& astarithmetic $< --hdu=$$n $$vsky - --output=$$tmpim     \
	&& astfits $$tmpim --copy=1 --output=$@                     \
	&& rm $$tmpim                                               \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/correct-sky.tex: $(csky-images)
	touch $@
