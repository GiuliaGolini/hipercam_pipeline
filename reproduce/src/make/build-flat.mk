# Build the flat
#
# This Makefile is written to build the flat for the raw images taken from HiPERCAM instrument.
# The main goal is to have a template to make the flat in each of the four window in every CCD cameras of HiPERCAM.
# This makefile firstly does the normalization of all extensions present in a HiPERCAM raw image.
# The normalization is done taking the median value of
# pixels in every hdu image and by dividing each hdu image for its median value. 
# To do that, a loop for reading all extensions is used.
# Then the makefile create a cube image containing the flat.
# The flat is created combining with a sigma clip mean the dithering images in each CCD's window.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Define the directory where the buid images are going to be
bf-outdir = $(BDIR)/flat





# Define the directory where the images from HiPERCAM corrected by bias are taken
bf-indir = $(BDIR)/corrected-bias




# Define a temporary directory where the imagesnormalized are
bf-tmpdir = $(BDIR)/bf-norm






# Define that all the bf-inputs are taken from the shell in the directory indir, all the files ending with .fits
bf-inputs = $(shell cd $(bf-indir) && ls *.fits)





# Now define a sub-target. To create the flat we need to use the normalized images.
# This is a loop to name the images: foreach rawimage in the set of rawimages writes in the outdirectory the name of the normalized image 
# by substituting the current name of the raw data to the extension _norm.fits.
bf-normalized-images = $(foreach bf-input,$(bf-inputs),$(bf-tmpdir)/$(subst .fits,_norm.fits,$(bf-input)))





# Normalization of the image 
# ----------------
#
# Use normalized images as variables to create the flat.
# To create the normalized images we need the raw ones taken from indirectory.
$(bf-tmpdir): ; mkdir $@
$(bf-normalized-images): $(bf-tmpdir)/%_norm.fits: $(bf-indir)/%.fits | $(bf-tmpdir)
	# The 3 following steps are needed to create images with the same raw images format: containing 20 sub-images corresponding to 4 window  
	# each filter (us gs  rs is zs).
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# normalized data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the image normalized
	# vnorm = normalization value obtained with the median
	# Normalize each image by dividing itself by vnorm
	# Inject the normalized data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 20);                                         \
	do                                                             \
	tmpim=$(subst _norm.fits,_norm-ext$$n.fits,$@)                 \
	&& vnorm=$$(aststatistics $< --hdu=$$n --median)               \
	&& astarithmetic $< --hdu=$$n $$vnorm / --output=$$tmpim       \
	&& astfits $$tmpim --copy=1 --output=$@                        \
	&& rm $$tmpim                                                  \
	; done


# Define the sub-targed needed to create the flat.
# To create the flats we use the normalized images.
$(bf-outdir): ; mkdir $@
$(bf-outdir)/flats.fits: $(bf-normalized-images) | $(bf-outdir)
	# The 3 following steps are needed to create images with the same raw images format:
	# 20 extensions (4 window each filter (us, gs, rs, is, zs).
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	
	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# Steps in the loop:
	# tmpim = temporal name for the flat image
	# Create the flat image by doing a sigma clip mean using all the normalized images extension
	# Inject the flat data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 20);                                                           \
	do                                                                               \
	tmpim=$(bf-outdir)/flat-$$n.fits                                                              \
	&& astarithmetic $^ -g$$n $(words $^) 3 0.2 sigclip-mean --output=$$tmpim        \
	&& astfits $$tmpim --copy=1 --output=$@                                          \
	&& rm $$tmpim                                                                    \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/build-flat.tex: $(bf-outdir)/flats.fits
	touch $@